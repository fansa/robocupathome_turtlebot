#!/usr/bin/env python
# -*- coding: utf-8 -*-
import roslib; roslib.load_manifest('speech')
import rospy
from std_msgs.msg import String
from std_msgs.msg import Int8
import os
from sound_play.libsoundplay import SoundClient

class nav_demo:
    def __init__(self):
        rospy.on_shutdown(self.cleanup)
        self.voice = rospy.get_param("~voice", "voice_cmu_us_clb_arctic_clunits")
        self.wavepath = rospy.get_param("~wavepath", "")
        self.soundhandle = SoundClient()
        rospy.sleep(1)
        self.soundhandle.stopAll()
        rospy.sleep(1)
        self.speech2nav_pub = rospy.Publisher('/speech2nav', String, queue_size=15)
        rospy.Subscriber('nav2speech',String,self.navCallback)
        os.system("flite -voice slt -t 'hello everyone'")
        rospy.sleep(1.2)
        os.system("flite -voice slt -t 'i will show you the navigation demo'")
        rospy.sleep(2.5)
        rospy.Subscriber('/recognizer_output',String,self.speechCallback)
        print "1111111111111111111111"


    def speechCallback(self, msg):
        print "3333333"
        msg.data=msg.data.lower()
        if msg.data == 'go':
            print "11111"
            self.speech2nav_pub.publish("point_one")
            os.system("flite -voice slt -t 'okay i will go to the point one'")
            #os.system("killall pocketsphinx_continuous")
            rospy.sleep(3)

    def navCallback(self, msg):
        if msg.data == "arrive":
            os.system("flite -voice slt -t 'i have arrived the point one'")
            rospy.sleep(3)
            os.system("flite -voice slt -t 'mission success'")
            rospy.sleep(2)


    def cleanup(self):
        rospy.loginfo("shuting down navsp node ....")


if __name__ == "__main__":
    rospy.init_node('nav_demo')
    try:
        nav_demo()
        rospy.spin()
    except:
        pass
