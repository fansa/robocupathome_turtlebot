#!/usr/bin/env python


import roslib; roslib.load_manifest('pi_speech_tutorial')
import rospy
import time
import os
from std_msgs.msg import String
from std_msgs.msg import Int8

from sound_play.libsoundplay import SoundClient

class restaurant:
	print (55955)
	def __init__(self):
		print 2222
		rospy.on_shutdown(self.cleanup)
		self.voice = rospy.get_param("~voice", "voice_cmu_us_clb_arctic_clunits")
		self.wavepath = rospy.get_param("~wavepath", "")
		self.soundhandle=SoundClient()
		rospy.sleep(1)
		self.soundhandle.stopAll()
		rospy.sleep(2)

		#record file
		self.filetime=str(time.localtime().tm_year)+'-'+str(time.localtime().tm_mon)+'-'+str(time.localtime().tm_mday)+'-'+str(time.localtime().tm_hour)+'-'+str(time.localtime().tm_min)+'restaurant'
		self.filepath=rospy.get_param("~filepath", "")
		self.recordfile = open(self.filepath+self.filetime+'.txt','w')
		
		self.myorder='' #one of two orders
		self.myorder1='' #another orders
		self.robot_name=rospy.get_param("~robot_name", "jack")
		
		self.r=0 #1 could start to get sentences robot hears
		self.l=0 #for detecting the left/right bar
		self.find_person=0 #1 means robot has noticed a person's waving hand
		self.chosen=0 #1 means this robot has been chosen
		self.fd=0 #1 means robot has arrived the table of calling person
		self.h=0 #1 means robot has arrived the bar with order
		self.twice=0 #1 means robot has token a order, and then it will take another order

		self.count_at_bar=0 #count the time return to the bar
		self.count_in_takeorder=0#count the time in function 'wait_choose' and in 'take-the-order'
		
		self.sp2nv=rospy.Publisher('speech2nav',String,queue_size=10)
		self.sp2img=rospy.Publisher('detectWave',String,queue_size=10)

		
		rospy.Subscriber('/barplace',String,self.say_back)
		rospy.Subscriber('voice_service',String,self.say_final)
		rospy.Subscriber('img2voice',String,self.say_find)
		rospy.Subscriber('recognizer_output',String,self.wait_choose)
		
		rospy.Subscriber('notice_person',String,self.saw_waving)

		self.soundhandle.say("dear operator",self.voice)
		rospy.sleep(1.8)
		self.soundhandle.say("please say",self.voice)
		rospy.sleep(1.3)
		self.soundhandle.say(self.robot_name,self.voice)
		rospy.sleep(1.5)
		self.soundhandle.say(" before your command",self.voice)
		rospy.sleep(2.5)
		self.soundhandle.say("now I am ready",self.voice)
		rospy.sleep(2)
		self.srt_turn('s')
		print 2345

	def srt_turn(self,data):#start
		self.record('command:start to detect bar')
		self.sp2nv.publish("start")

	def say_back(self,msg):#detect bar
		print 22222
		msg.data=msg.data.lower()
		print msg.data
		if msg.data.find('left')>-1:
			self.soundhandle.say('the bar is on the left',self.voice)
			rospy.sleep(4)
		if msg.data.find('right')>-1:
			self.soundhandle.say('the bar is on the right',self.voice)
			rospy.sleep(4)
		self.record('command:the bar is on the '+msg.data)
		self.soundhandle.say('dear guest please wave your hand beside your face',self.voice)
		rospy.sleep(5)
		self.sp2img.publish('wave')
		

	def wait_choose(self,msg):#if the robot was chosen
		print(9999)
		msg.data=msg.data.lower()
		print msg.data
		if (self.count_at_bar==3 or (self.find_person==1 and self.myorder=='') )and self.count_in_takeorder==0:
			if self.r==1:
				self.record('voice:'+msg.data)
				print('in wait_choose')
				if msg.data.find('take-the-order')>-1 and msg.data.find(self.robot_name)>-1 and msg.data.find('take-the-order')>msg.data.find(self.robot_name):
					print('in take the order')
					self.count_in_takeorder=1
					self.chosen=1
					self.soundhandle.say('Ok I will take order',self.voice)
					rospy.sleep(4)
					self.record('command:robot will take order')
					self.sp2nv.publish('go2person')
				elif msg.data.find(self.robot_name+' wait')>-1 and msg.data.find(self.robot_name)>-1 and msg.data.find('wait')>msg.data.find(self.robot_name) :
					self.chosen=0
					self.soundhandle.say('Ok I will wait for your next order',self.voice)
					self.record('command:robot will wait for your next order')
					rospy.sleep(5)
				elif msg.data.find('stop-the-test')>-1 and msg.data.find(self.robot_name)>-1 and msg.data.find('stop-the-test')>msg.data.find(self.robot_name):
					self.lock_them(self)
					self.soundhandle.say('Ok I will stop',self.voice)
					self.record('command:robot will stop')
					rospy.sleep(5)
				elif msg.data.find('find-a-new-client-to-serve')>-1 and msg.data.find(self.robot_name)>-1 and msg.data.find('find-a-new-client-to-serve')>msg.data.find(self.robot_name):
					self.init(self)
					self.soundhandle.say('Ok I will find a new client to serve',self.voice)
					self.record('command:robot will find a new client to serve')
					rospy.sleep(5)
					self.sp2img.publish('wave')

	def saw_waving(self,msg):#saw a waving person
		if self.find_person==0:
			self.find_person=1
			self.sp2img.publish('stopvel')
			self.record('command:robot have found the person who is waving hand')
			self.soundhandle.say('I have found the person who is waving hand',self.voice)
			rospy.sleep(5)
			
			self.r=1;

	def say_find(self,msg):#arrived at the table of calling person
		print(777)
		msg.data=msg.data.lower()
		print msg.data
		if self.fd==0:
			self.soundhandle.say('I have arrived the table of the calling person',self.voice)
			rospy.sleep(4)
			self.soundhandle.say("please say",self.voice)
			rospy.sleep(2)
			self.soundhandle.say(self.robot_name,self.voice)
			rospy.sleep(1.5)
			self.soundhandle.say("before your    order",self.voice)
			rospy.sleep(2.5)
			self.soundhandle.say('now please say your order',self.voice)
			rospy.sleep(2)
			self.r=1
			self.fd=1
			rospy.Subscriber('recognizer_output',String,self.say_start)
			self.record('command:robot have arrived the table of the calling person and ready to take order')
			self.sp2nv.publish("order1")

				

	def say_start(self,msg):#get the order
		msg.data=msg.data.lower()
		
		print msg.data
		if self.r==1 and self.find_person==1 and self.chosen==1 and msg.data.find(self.robot_name)>-1 and self.fd==1 and self.h==0:
			self.sp2img.publish('stop')
			self.record('voice:'+msg.data)
			print ("self.r=",self.r,"   self.l=",self.l,"    self.find_person=",self.find_person,"    self.chosen=",self.chosen,"    self.fd=",self.fd,"    self.h=",self.h,"   self.twice=",self.twice,'     myorder='+self.myorder+'   myorder1='+self.myorder1)
			if msg.data.find('green-tea')>-1:
				if self.twice==0:
					self.myorder='green tea'
					
				elif self.twice==1:
					self.myorder1='green tea'
					self.r=2
				self.soundhandle.say('I have taken your order       ',self.voice)
				rospy.sleep(3)
				if self.twice==0:
					self.soundhandle.say(self.myorder,self.voice)
					self.record('command:robot has recored A\'s  order:'+self.myorder)
				elif self.twice==1:
					self.soundhandle.say(self.myorder1,self.voice)
					self.record('command:robot has recored B\'s  order:'+self.myorder1)
				rospy.sleep(3)
				self.twice=self.twice+1
				
			elif msg.data.find('potato-chips')>-1:
				
				if self.twice==0:
					self.myorder='potato-chips'
					
				elif self.twice==1:
					self.myorder1='potato-chips'
					self.r=2
				self.soundhandle.say('I have taken your order       ',self.voice)
				rospy.sleep(3)
				if self.twice==0:
					self.soundhandle.say(self.myorder,self.voice)
					self.record('command:robot has recored A\'s  order:'+self.myorder)
				elif self.twice==1:
					self.soundhandle.say(self.myorder1,self.voice)
					self.record('command:robot has recored B\'s  order:'+self.myorder1)
				rospy.sleep(3)
				self.twice=self.twice+1
				
			
			if self.twice==2:
				self.sp2nv.publish('backtobar')
				self.record('command:robot is ready to go back to bar')

				
	def say_final(self,msg):#have reached bar and talk to barman
		self.sp2img.publish('stop')
		if self.h == 0:
			self.h=1
			rospy.sleep(1)
			self.soundhandle.say('I have reached the   bar table',self.voice)
			rospy.sleep(3)
			self.soundhandle.say('the person A wants      ',self.voice)
			rospy.sleep(2.5)
			self.soundhandle.say(self.myorder,self.voice)
			rospy.sleep(2.5)
			self.soundhandle.say('the person B wants      ',self.voice)
			rospy.sleep(2.5)
			self.soundhandle.say(self.myorder1,self.voice)
			rospy.sleep(2.5)
			self.count_at_bar=self.count_at_bar+1
			self.record('command:robot have reached the bar table and ready to take object for A')
			
		if self.count_at_bar==1 and msg.data.find('reachbar')>-1:
			rospy.sleep(10)
			self.soundhandle.say("i have took-"+self.myorder+'-for A',self.voice)
			rospy.sleep(4)
			self.record('command:robot has take object for A')
			self.sp2nv.publish('backorder1')
		elif self.count_at_bar==1 and msg.data.find('reachclient1')>-1:
			rospy.sleep(10)
			self.soundhandle.say('i have arrived client A',self.voice)
			rospy.sleep(4)
			self.record('command:robot has delivered object to A')
			self.sp2nv.publish('backtobar')
			self.count_at_bar=self.count_at_bar+1
		elif self.count_at_bar==2 and msg.data.find('reachbar')>-1:
			rospy.sleep(10)
			self.soundhandle.say("i have took-"+self.myorder+'-for B',self.voice)
			rospy.sleep(4)
			self.record('command:robot has take object for B')
			self.sp2nv.publish('backorder1')
		elif self.count_at_bar==2 and msg.data.find('reachclient1')>-1:
			rospy.sleep(10)
			self.soundhandle.say('i have arrived client B',self.voice)
			rospy.sleep(4)
			self.record('command:robot has delivered object to B')
			self.sp2nv.publish('backtobar')
			self.count_at_bar=self.count_at_bar+1
		elif self.count_at_bar==3 and msg.data.find('reachbar')>-1:
			self.soundhandle.say('i have completed my mission and i will wait here',self.voice)
			self.record('command:robot has completed mission and will wait here')
			self.count_in_takeorder=0
			self.r=1
			rospy.sleep(5)
			

	def cleanup(self):
		rospy.loginfo("Shutting down restaurant node...")
		self.recordfile.close()

	def init(self,msg):
		self.r=1 #1 could start to get sentences robot hears
		self.l=0 #for detect the left/right bar
		self.find_person=0 #1 means robot has noticed a person's waving hand
		self.chosen=0 #1 means this robot has been chosen
		self.fd=0 #1 means robot has arrived the table of calling person
		self.h=0 #1 means robot has arrived the bar with order
		self.count_at_bar=0
		self.myorder='' #one of two orders
		self.myorder1='' #another orders
		self.twice=0
		self.count_at_bar=0

	def lock_them(self,msg):
		self.r=2
		self.find_person=2
		self.chosen=2
		self.fd=2
		self.h=2

	def record(self,com):
		self.commandtime=str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)+':'+str(time.localtime().tm_sec)
		self.recordfile.write(self.commandtime+'----'+com+'\n')
		
if __name__=="__main__":
	rospy.init_node('restaurant')
	try:
		restaurant()
		rospy.spin()
		
	except:
		pass
