#!/usr/bin/env python


"""
    navigation.py - Say back what is heard by the pocketsphinx recognizer.
"""

import roslib; roslib.load_manifest('speech')
import rospy
from std_msgs.msg import String
from std_msgs.msg import Int8
import os

from sound_play.libsoundplay import SoundClient

class help_me_carry:

	def __init__(self):

		rospy.on_shutdown(self.cleanup)
		self.voice = rospy.get_param("~voice", "voice_cmu_us_clb_arctic_clunits")
		self.wavepath = rospy.get_param("~wavepath", "")
		self.state="true"
		self.soundhandle=SoundClient()
		rospy.sleep(1)
		self.soundhandle.stopAll()
		rospy.sleep(1)
		self.soundhandle.say("ready",self.voice)
		rospy.sleep(1.1)
		self.pub = rospy.Publisher('/ifFollowme', String, queue_size=15)
		self.loc_pub = rospy.Publisher('voice2bring', String, queue_size=15)
		self.srt_guide = rospy.Publisher('voice2guide', String, queue_size=15)
		self.inspect = rospy.Publisher('go_out', String, queue_size=15)


		rospy.Subscriber('inspect2speech',String,self.inspect_callback)
		rospy.Subscriber('/emergency2speech',String,self.emergency_callback)		

		self.difmsg='null'
		self.if_followme=0
		self.if_stop=0
		self.if_locpub=0
		self.say_time=0

		os.system("pocketsphinx_continuous -inmic yes -dict /home/kamerider/catkin_ws/src/speech/voice_library/inspect/1131.dic -lm /home/kamerider/catkin_ws/src/speech/voice_library/inspect/1131.lm;")
		rospy.Subscriber('recognizer_output',String,self.go_back)
	def emergency_callback(self,msg):
		msg.data=msg.data.lower()
		if msg.data=="true":
			self.state="true"
		else :
			self.state="false"
			self.inspect.publish('false')
			print "2222222222222222222222"


	def just_say(self,msg):
		msg.data=msg.data.lower()
		self.soundhandle.say( msg.data,self.voice)
		rospy.sleep(4)



	def inspect_callback(self,msg):
		msg.data=msg.data.lower()
		if msg.data=="get_pose":
			os.system("/home/kamerider/catkin_ws/src/speech/kill_pocketsphinx.sh")
			self.soundhandle.say(" i have reached the position ",self.voice)
			rospy.sleep(2)
			self.soundhandle.say(" please check me ",self.voice)
			rospy.sleep(3)
			self.soundhandle.say(" when the check is over please say go out",self.voice)
			rospy.sleep(5)
			os.system("pocketsphinx_continuous -inmic yes -dict /home/kamerider/catkin_ws/src/speech/voice_library/inspect/1131.dic -lm /home/kamerider/catkin_ws/src/speech/voice_library/inspect/1131.lm")
	def go_back(self,msg):
		msg.data=msg.data.lower()
		print msg.data
		if msg.data.find('out') > -1 or msg.data.find('go') > -1 or msg.data.find('go-out') > -1 :
			self.inspect.publish('go_out')
	
	def cleanup(self):
		rospy.loginfo("shuting down navsp node ....")
if __name__=="__main__":
	rospy.init_node('help_me_carry')
	try:
		help_me_carry()
		rospy.spin()
	except:
		pass





